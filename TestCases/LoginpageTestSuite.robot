*** Settings ***
Library    Selenium2Library
Resource   ../Keywords/Resource.robot
Resource   ../Keywords/Param.robot

Suite Setup    Init CSS and HTML Variables

Test Setup    Login Page Sould be Alive
Test Teardown    Close All Browsers

*** Test Cases ***
User Should Be Able to Login with Valid Username And Password
    [Documentation]    Login index with valid username And Password should pass
    [Tags]    Regression
    Login With Valid Username And Password
    Check If There Is Page Error
    Should See Success Login
    Capture Page Screenshot

User Should Not Able to Login with Valid Username And Wrong Password
    [Documentation]    Login with wrong password should failed
    [Tags]    Regression
    Login With Valid Username And Wrong Password
    Should See Login Failed with Wrong Password
    Capture Page Screenshot

User Should Not Able to Login with invalid Username
    [Documentation]    Login with invalid username
    [Tags]    Regression
    Login With Invalid Username
    Should See Login Failed with Invalid username
    Capture Page Screenshot

User Should Be Able to Logout
    [Documentation]    Login user should be able to logout
    [Tags]    Regression
    Login With Valid Username And Password
    Should See Success Login
    Click Logout
    Should be Able to see Login Page Again
    Capture Page Screenshot
