*** Keywords ***
Init CSS and HTML Variables
    Set Suite Variable    ${login_password_block}    password
    Set Suite Variable    ${login_username_block}    username
    Set Suite Variable    ${login_success}    success
    Set Suite Variable    ${login_error}    error
    Set Suite Variable    ${login_button}    fa-sign-in
    Set Suite Variable    ${logout_button}    icon-signout
