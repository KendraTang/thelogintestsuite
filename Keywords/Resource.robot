*** Settings ***
Library    Selenium2Library
Resource   Param.robot

*** Variables ***
${webpage_host}    https://the-internet.herokuapp.com/login
${valid_username}    tomsmith
${valid_password}    SuperSecretPassword!
${invalid_username}    user
${invalid_password}    admin
${login_success_wording}    You logged into a secure area!
${wrong_password_wording}     Your password is invalid!
${invalid_user_wording}     Your username is invalid!
${Browser}  chrome

*** Keywords ***
Login Page Sould be Alive
    Open Custome Browser to Webpage Host    ${webpage_host}
    Check If There Is Page Error
    Check If Page Status Is Loginable

Open Custome Browser to Webpage Host
    [Arguments]    ${webpage_host}
    ${chrome_options}=    Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --window-size\=1920,1080
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --ignore-ssl-errors
    Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    Create Webdriver  Chrome    chrome_options=${chrome_options}
    Go to   ${webpage_host}

Check If Page Status Is Loginable
    [Documentation]    Check if the webpage is loginable
    Element Should be Visible    id=${login_username_block}
    Element Should be Visible    id=${login_password_block}
    Element Should be Visible    class=${login_button}
    Log to console    Page status is loginable

Login With Valid Username And Password
    Login With Username And Password    ${valid_username}    ${valid_password}

Login With Valid Username And Wrong Password
    Login With Username And Password    ${valid_username}    ${invalid_password}

Login With Invalid Username
    Login With Username And Password    ${invalid_username}    ${invalid_password}

Login With Username And Password
    [Arguments]    ${username}    ${password}
    Wait And Input    id=${login_username_block}    ${username}
    Wait And Input    id=${login_password_block}    ${password}
    Click Login Button

Click Login Button
    Wait And Click    class=${login_button}

Should See Success Login
    Element Should be Visible    class=${login_success}
    Wait Until Page Contains    ${login_success_wording}

Should See Login Failed with Wrong Password
    Element Should be Visible    class=${login_error}
    Wait Until Page Contains    ${wrong_password_wording}

Should See Login Failed with Invalid username
    Element Should be Visible    class=${login_error}
    Wait Until Page Contains    ${invalid_user_wording}

Click Logout
    Wait And Click    class=${logout_button}

Should be Able to see Login Page Again
    Check If Page Status Is Loginable

Wait And Input
    [Arguments]  ${locator}  ${text}
    Wait Until Element Is Visible    ${locator}    timeout=10
    Input Text  ${locator}  ${text}

Wait And Click
    [Arguments]  ${locator}
    Wait Until Element Is Visible    ${locator}    timeout=10
    Click Element  ${locator}

Check If There Is Page Error
     Sleep    1
     Element Should Not Be Visible    class=el-message__content
